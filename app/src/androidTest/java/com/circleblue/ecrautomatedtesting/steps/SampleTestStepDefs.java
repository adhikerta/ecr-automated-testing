package com.circleblue.ecrautomatedtesting.steps;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.uiautomator.UiDevice;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class SampleTestStepDefs extends BaseStepDefs {
  @Given("User is at home screen")
  public void userIsAtHomeScreen() {
    if (mDevice == null)
      mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

    assertThat(mDevice, notNullValue());
    mDevice.pressHome();
  }

  @Then("starts ECR app")
  public void startsECRApp() {
    startMainActivityFromHomeScreen();
    assertEquals(mDevice.getCurrentPackageName(), getDefaultActivityPackage());
  }

  @And("create new user {string}")
  public void createNewUser(String name) {
    // let's type something on create new user screen
    viewObjectSetText("com.circleblue.ecr:id/usernameTextInputEditText", "Mr Niko");

    // click on next button
    getViewObject("com.circleblue.ecr:id/buttonNext").click();
  }

  @And("set new PIN to {string}")
  public void setNewUserPin(String pin) {
    // still on the same activity but different frame with title: "Enter your new PIN code"
    assertEquals(
        getViewObject("com.circleblue.ecr:id/authTitleTextView").getText(),
        "Enter your new PIN code");

    // type PIN
    for (char ch : pin.toCharArray()) {
      String viewTarget = "com.circleblue.ecr:id/key_" + ch + "_button";
      getViewObject(viewTarget).click();
    }
  }

  @Override
  public String getDefaultActivityPackage() {
    return "com.circleblue.ecr";
  }
}
