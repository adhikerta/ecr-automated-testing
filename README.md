# Android Instrumented Unit Tests for ECR app

## Using Android Studio 

Install the following plugins:

- Cucumber for Java,
- Gherkin,
- Google Java Format,

and make sure to disable *Substeps IntelliJ* plugin as stated [here](https://stackoverflow.com/questions/45929639/i-keep-getting-the-error-unimplemented-substep-definition-in-intellij-with-cu) because it can generate errors on .feature files (and Gherkin syntax).

---

## Making Instrumented Unit Tests

### 1. Connect your Android device

Make sure to enable USB Debugging on your device and it is listed on Android Studio when you connect it to your computer by using a USB cable.
You can also install *WiFi ADB Ultimate* plugin to debug your device using WiFi network, but first you have to connect it using USB cable and run `adb tcpip <port>`. 
More details [here](https://android.jlelse.eu/wireless-debugging-through-adb-in-android-using-wifi-965f7edd163a).

### 2. UI Automator Viewer

UI Automator Viewer can be used to capture current device's screen and get properties of the current activity and it's view elements. 

![UI Automator Viewer](uiautomatorviewer.png)

Open `ANDROID_SDK/tools/bin/uiatomatorviewer.bat` (Windows) or `ANDROID_SDK/tools/bin/uiatomatorviewer` (Mac) 
to run the application. `ANDROID_SDK` refers to your Android SDK installation - it usually located 
in `C:\Users\[your_user]\AppData\Local\Android\Sdk` (Windows) or `/Library/Android/sdk/` (Mac).

### 3. Making The Tests

![Folder structure](folder.png)

Place your Cucumber feature files in `src/androidTest/assets/features` folder. Cucumber for Java plugin will allow you to use CTRL + click (Windows) or CMD + click (Mac) in order to navigate to the step definition class and the handler method itself.

You can also create new method (or class) for undefined step references, by typing ALT + Enter on the undefined step sentence, which will open the context menu:

![Context menu](menu.png)

Make sure to place the step definition classes in `src/java/com/circleblue/ecrautomatedtesting/steps` folder, as it was configured for `glue` on Cucumber options annotation at `ECRJUnitRunner` class.  

`ECRJUnitRunner` is our test instrumentation runner which uses [Cucumber-Android library](https://github.com/cucumber/cucumber-android).

### 4. Run or Debug Tests

To run or debug tests using Android Studio, you'll need to create a run/debug configuration. 
Click `Run - Edit Configurations...` menu, and create a new `Android Instrumented Tests`. 
On `General` tab, choose `app` for `Module` selection.

![Run configuration](config.png)

--- 

## Codes Format

Once *Google Java Format* plugin is activated, format your Java (.java) and Cucumber feature files (.feature) 
by using CTRL + Alt + L (Windows) or CMD + Alt + L (Mac). 
Make sure to optimize imports (remove unused imports) as well, by using CTRL + Alt + O (Windows) or CMD + Alt + O (Mac).